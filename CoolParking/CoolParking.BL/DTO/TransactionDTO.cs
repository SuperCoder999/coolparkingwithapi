using System;
using Newtonsoft.Json;
using CoolParking.BL.Json;

namespace CoolParking.BL.DTO
{
    public struct TransactionTopUpDTO
    {
        [JsonRequired]
        [JsonProperty("id")]
        public string VehicleId { get; set; }

        [JsonRequired]
        [JsonProperty("Sum")]
        [JsonConverter(typeof(StrictIntegralJsonConverter))]
        public decimal Sum { get; set; }
    }

    public struct TransactionOutputDTO
    {
        [JsonProperty("vehicleId")]
        public string VehicleId { get; set; }

        [JsonProperty("sum")]
        public decimal Sum { get; set; }

        [JsonProperty("transactionDate")]
        public DateTime Date { get; set; }
    }
}
