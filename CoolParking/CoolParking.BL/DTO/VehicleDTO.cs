using System;
using Newtonsoft.Json;
using CoolParking.BL.Models;
using CoolParking.BL.Json;

namespace CoolParking.BL.DTO
{
    public struct VehicleInputDTO
    {
        [JsonRequired]
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonRequired]
        [JsonProperty("vehicleType")]
        [JsonConverter(typeof(StrictIntegralJsonConverter))]
        public VehicleType VehicleType
        {
            get => _type;
            set
            {
                int maxTypeNumber = Enum.GetNames(typeof(VehicleType)).Length - 1;

                if ((int)value > maxTypeNumber || (int)value < 0)
                {
                    throw new FormatException($"vehicleType must be a number from 0 to {maxTypeNumber}");
                }

                _type = value;
            }
        }

        [JsonIgnore]
        private VehicleType _type;

        [JsonRequired]
        [JsonProperty("balance")]
        [JsonConverter(typeof(StrictIntegralJsonConverter))]
        public decimal Balance { get; set; }
    }

    public struct VehicleOutputDTO
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("vehicleType")]
        public VehicleType VehicleType { get; set; }

        [JsonProperty("balance")]
        public decimal Balance { get; set; }

        public override string ToString()
        {
            return $"{Vehicle.GetStringType(VehicleType)} registered as {Id} with balance {Balance}";
        }
    }
}
