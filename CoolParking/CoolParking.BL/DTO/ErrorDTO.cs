using Newtonsoft.Json;

namespace CoolParking.BL.DTO
{
    public struct ErrorDTO
    {
        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
