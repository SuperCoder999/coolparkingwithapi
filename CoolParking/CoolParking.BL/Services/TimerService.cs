﻿using System.Timers;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public sealed class TimerService : ITimerService
    {
        public event ElapsedEventHandler Elapsed;

        public double Interval
        {
            get => timer.Interval / 1000;
            set => timer.Interval = value * 1000;
        }

        private readonly Timer timer;

        public TimerService(double interval)
        {
            timer = new Timer();
            Interval = interval;
            timer.Enabled = false;
            timer.Elapsed += FireElapsedEvent;
        }

        public void Start()
        {
            timer.Enabled = true;
        }

        public void Stop()
        {
            timer.Enabled = false;
        }

        public void Dispose()
        {
            Stop();
            timer.Dispose();
        }

        private void FireElapsedEvent(object sender, ElapsedEventArgs args)
        {
            Elapsed?.Invoke(this, args);
        }
    }
}
