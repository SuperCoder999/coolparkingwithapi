using System;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Validators
{
    public sealed class HttpStatusValidator : IValidator<int>
    {
        public void Validate(int status)
        {
            if (status < 100 || status > 599)
            {
                throw new ArgumentException(Settings.validationErrorMessages["invalidHttpStatus"], nameof(status));
            }
        }
    }
}
