using System;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Validators
{
    public sealed class PositiveIntValidator : IValidator<int>
    {
        public void Validate(int number)
        {
            if (number < 0)
            {
                throw new ArgumentException(Settings.validationErrorMessages["negativeInteger"], nameof(number));
            }
        }
    }
}
