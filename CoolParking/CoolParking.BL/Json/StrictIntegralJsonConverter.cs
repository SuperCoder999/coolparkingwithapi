using System;
using Newtonsoft.Json;

namespace CoolParking.BL.Json
{
    internal sealed class StrictIntegralJsonConverter : JsonConverter
    {
        // public StrictIntegralJsonConverter(params Type[] types) : base() { }
        private readonly JsonSerializer defaultSerializer = new JsonSerializer();

        public override bool CanWrite
        {
            get => false;
        }

        public override bool CanConvert(Type type)
        {
            return type.IsIntegralType();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type type, object value, JsonSerializer serializer)
        {
            switch (reader.TokenType)
            {
                case JsonToken.Integer:
                case JsonToken.Float:
                case JsonToken.Null:
                    return defaultSerializer.Deserialize(reader, type);
                default:
                    throw new JsonSerializationException($"Token \"{reader.Value}\" of type {reader.TokenType} was not a JSON integer");
            }
        }
    }
}
