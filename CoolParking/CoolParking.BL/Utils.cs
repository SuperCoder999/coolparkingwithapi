using System;
using Newtonsoft.Json;
using CoolParking.BL.Models;

namespace CoolParking.BL
{
    public static class Utils
    {
        private static readonly Random random = new Random();

        // Random functions
        public static int RandomInt(int min, int max)
        {
            return random.Next(min, max + 1);
        }

        public static char RandomLetter(bool uppercase = false)
        {
            int index = random.Next(0, Settings.englishAlphabet.Length);
            char letter = Settings.englishAlphabet[index];

            if (uppercase)
            {
                return letter.ToString().ToUpper()[0];
            }

            return Settings.englishAlphabet[index];
        }

        public static string RandomString(int length = 2, bool uppercase = false)
        {
            string result = "";

            for (int i = 0; i < length; i++)
            {
                result += RandomLetter(uppercase);
            }

            return result;
        }

        public static string RandomDigitString(int min, int max, int length = 2)
        {
            string result = "";

            for (int i = 0; i < length; i++)
            {
                result += RandomInt(min, max);
            }

            return result;
        }

        // Attribute functions
        public static TAttr GetSingleAttribute<TAttr, TClass>(bool inherit = false) where TAttr : Attribute
        {
            TAttr[] attributeArray = GetMultipleAttribute<TAttr, TClass>(inherit);

            if (attributeArray.Length < 1)
            {
                return null;
            }

            return attributeArray[0];
        }

        public static TAttr[] GetMultipleAttribute<TAttr, TClass>(bool inherit = false) where TAttr : Attribute
        {
            return (TAttr[])typeof(TClass).GetCustomAttributes(typeof(TAttr), inherit);
        }

        // String functions
        public static string CleanString(string str)
        {
            return str.ToLower().Replace(" ", "");
        }

        // Json functions
        public static string JsonStringify(object value)
        {
            return JsonConvert.SerializeObject(value);
        }

        public static T JsonParse<T>(string value)
        {
            return JsonConvert.DeserializeObject<T>(value);
        }

        public static object JsonParse(string value, Type type)
        {
            return JsonConvert.DeserializeObject(value, type);
        }
    }

    public static class TypeExtensions
    {
        public static bool IsIntegralType(this Type type)
        {
            type = Nullable.GetUnderlyingType(type) ?? type;

            if (type == typeof(long)
                || type == typeof(ulong)
                || type == typeof(int)
                || type == typeof(uint)
                || type == typeof(short)
                || type == typeof(ushort)
                || type == typeof(byte)
                || type == typeof(sbyte)
                || type == typeof(System.Numerics.BigInteger))
            {
                return true;
            }

            return false;
        }
    }
}
