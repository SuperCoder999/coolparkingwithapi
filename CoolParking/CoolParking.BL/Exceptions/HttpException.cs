using System;
using Newtonsoft.Json;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Validators;

namespace CoolParking.BL.Exceptions
{
    public class HttpException : Exception
    {
        public int Status { get; }
        private static readonly IValidator<int> statusValidator = new HttpStatusValidator();

        public HttpException(string message) : base(message)
        {
            Status = 500;
        }

        public HttpException(int status, string message) : base(message)
        {
            statusValidator.Validate(status);
            Status = status;
        }

        public static HttpException From(Exception exc)
        {
            if (exc is HttpException)
            {
                return exc as HttpException;
            }
            else if (exc is ArgumentException
                || exc is InvalidOperationException
                || exc is FormatException
                || exc is JsonException)
            {
                return new HttpException(400, exc.Message);
            }

            return new HttpException(exc.Message);
        }
    }
}
