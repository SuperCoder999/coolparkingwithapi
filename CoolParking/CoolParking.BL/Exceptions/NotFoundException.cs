namespace CoolParking.BL.Exceptions
{
    public sealed class NotFoundException : HttpException
    {
        public NotFoundException(string entity) : base(404, $"{entity} is not found") { }
    }
}
