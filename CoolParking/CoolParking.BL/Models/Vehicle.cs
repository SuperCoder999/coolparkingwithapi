﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using CoolParking.BL.Validators;
using CoolParking.BL.DTO;

namespace CoolParking.BL.Models
{
    public sealed class Vehicle
    {
        public string Id { get; private set; }
        public VehicleType VehicleType { get; private set; }
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType type, decimal balance)
        {
            this.Id = id;
            this.VehicleType = type;
            this.Balance = balance;

            new VehicleValidator().Validate(this);
        }

        public static Vehicle FromDTO(VehicleInputDTO dto)
        {
            return new Vehicle(dto.Id, dto.VehicleType, dto.Balance);
        }

        public static VehicleOutputDTO ToDTO(Vehicle vehicle)
        {
            return new VehicleOutputDTO
            {
                Id = vehicle.Id,
                Balance = vehicle.Balance,
                VehicleType = vehicle.VehicleType,
            };
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            string startLetters = Utils.RandomString(2, true);
            string endLetters = Utils.RandomString(2, true);
            string fourDigits = Utils.RandomDigitString(0, 9, 4);

            return $"{startLetters}-{fourDigits}-{endLetters}";
        }

        public static string GetStringType(VehicleType type)
        {
            switch (type)
            {
                case VehicleType.Bus:
                    return "Bus";
                case VehicleType.Motorcycle:
                    return "Motorcycle";
                case VehicleType.PassengerCar:
                    return "Passenger car";
                case VehicleType.Truck:
                    return "Truck";
                default:
                    return "";
            }
        }

        public static VehicleType GetTypeByCleanedString(string type)
        {
            switch (type)
            {
                case "bus":
                    return VehicleType.Bus;
                case "motorcycle":
                    return VehicleType.Motorcycle;
                case "passengercar":
                    return VehicleType.PassengerCar;
                case "truck":
                    return VehicleType.Truck;
                default:
                    throw new ArgumentException(Settings.errorMessages["invalidVehicleType"], nameof(type));
            }
        }

        public override string ToString()
        {
            return $"{GetStringType(VehicleType)} registered as {Id} with balance {Balance}";
        }

        public VehicleOutputDTO ToDTO()
        {
            return Vehicle.ToDTO(this);
        }
    }
}
