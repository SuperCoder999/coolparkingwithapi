﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static readonly decimal startParkingBalance = 0m;
        public static readonly int parkingCapacity = 10;
        public static readonly float paymentWithdrawDelaySecs = 5f;
        public static readonly float transactionLoggingDelaySecs = 60f;
        public static readonly decimal fineCoefficient = 2.5m;
        public static readonly Regex idValidationRegex = new Regex(@"^[A-Z]{2}-\d{4}-[A-Z]{2}$");
        public static readonly string menuExitCommand = "exit";
        public static readonly string englishAlphabet = "abcdefghijklmnopqrstuvwxyz";
        public static readonly string logfilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}/Transactions.log";
        public static readonly string noTransactionsStub = "[No transactions to log]";
        public static readonly string baseUrl = "http://localhost:5000";

        public static readonly Dictionary<VehicleType, decimal> vehiclePaymentRate = new Dictionary<VehicleType, decimal>() {
            { VehicleType.Bus, 3.5m },
            { VehicleType.Motorcycle, 1m },
            { VehicleType.PassengerCar, 2m },
            { VehicleType.Truck, 5m }
        };

        public static readonly Dictionary<string, string> errorMessages = new Dictionary<string, string>() {
            { "notExistingFile", "File does not exist" },
            { "existingVehicleId", "This vehicle ID already exists" },
            { "noPlaces", "No free places in parking" },
            { "vehicleNotFound", "Vehicle is not found" },
            { "cantRemoveNoMoney", "Can't remove vehicle due to negative balance" },
            { "notFoundParameter", "Trying to get unexisting parameter" },
            { "notFoundCommand", "Trying to execute unexisting command" },
            { "noUseCommandAttribute", "Command doesn't have Command attribute" },
            { "noParamResultValue", "Trying to get value from CommandParameterResult, which hasn't got value" },
            { "illegalSingletonCreation", "Can't execute Singleton's constructor, because of different data passed to it" },
        };

        public static readonly Dictionary<string, string> validationErrorMessages = new Dictionary<string, string>() {
            { "invalidBalance", "Balance should be greater or equal to 0" },
            { "negativeInteger", "Negative integers aren't allowed" },
            { "invalidID", "ID should match the following pattern: XX-YYYY-XX, where X is any uppercase English letter and Y is any digit" },
            { "invalidHttpStatus", "Http status should an integer, greater than 99 and less than 600" },
            { "nullVehicle", "Vehicle object can't be null" },
        };

        public static readonly Dictionary<string, string> paramErrorMessages = new Dictionary<string, string>() {
            { "logTransactionsCount", "Enter a valid positive integer or leave the field blank" },
            { "vehicleType", "Can't find this vehicle type" },
            { "vehicleBalance", "Enter a valid positive number" },
            { "newVehicleBalance", "Enter a valid positive number" },
        };

        public static readonly Dictionary<string, string> paramMessages = new Dictionary<string, string>() {
            { "logTransactionsCount", "Enter count of last transactions to log or leave blank to log all: " },
            { "vehicleType", "Enter vehicle type (Bus, Passenger car, Motorcycle or Truck): " },
            { "vehicleBalance", "Enter positive vehicle balance: " },
            { "vehicleId", "Enter vehicle ID: " },
            { "newVehicleBalance", "Enter positive number to add to vehicle balance: " },
        };

        public static readonly Dictionary<string, string> commandMessages = new Dictionary<string, string>() {
            { "getParkingBalance", "Get parking balance" },
            { "getLastTopUpSum", "Get last transactions sum" },
            { "getPlacesInfo", "Get parking places info" },
            { "getLastTransactions", "Get all info about last parking transactions" },
            { "getLogTransactions", "Get transactions info from log file" },
            { "getRegisteredVehicles", "Get info about registered vehicles" },
            { "addVehicle", "Add vehicle to parking" },
            { "removeVehicle", "Remove vehicle by ID" },
            { "topUpVehicle", "Top up vehicle by ID" },
        };
    }
}
