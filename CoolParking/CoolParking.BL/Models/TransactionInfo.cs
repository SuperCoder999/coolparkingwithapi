﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;
using CoolParking.BL.DTO;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public string VehicleId { get; }
        public decimal Sum { get; }
        public DateTime Date { get; }

        public TransactionInfo(string vehicleId, decimal sum)
        {
            Sum = sum;
            VehicleId = vehicleId;
            Date = DateTime.Now;
        }

        private TransactionInfo(string vehicleId, decimal sum, DateTime customDate)
        {
            Sum = sum;
            VehicleId = vehicleId;
            Date = customDate;
        }

        public static TransactionOutputDTO ToDTO(TransactionInfo trans)
        {
            return new TransactionOutputDTO
            {
                VehicleId = trans.VehicleId,
                Sum = trans.Sum,
                Date = trans.Date,
            };
        }

        public static TransactionInfo FromDTO(TransactionOutputDTO dto)
        {
            return new TransactionInfo(dto.VehicleId, dto.Sum, dto.Date);
        }

        public override string ToString()
        {
            return $"[{Date}] Withdrawn vehicle registered as {VehicleId}. Amount: {Sum}";
        }

        public TransactionOutputDTO ToDTO()
        {
            return TransactionInfo.ToDTO(this);
        }
    }
}
