﻿using System.Threading.Tasks;

namespace CoolParking.Client
{
    public sealed class Program
    {
        public static async Task Main(string[] args)
        {
            ConsoleMenu app = new ConsoleMenu();
            await app.Run();
        }
    }
}
