using System;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using CoolParking.Client.CommandLogic;

namespace CoolParking.Client
{
    public sealed class ConsoleMenu
    {
        private ICommandService commander;

        public ConsoleMenu()
        {
            commander = new CommandService();
        }

        public ConsoleMenu(ICommandService commandService)
        {
            commander = commandService;
        }

        public async Task Run()
        {
            await PrintAndAsk();
        }

        private async Task PrintAndAsk()
        {
            Console.Write(commander.GetCommandsStringInfo());
            Console.WriteLine($"(Enter command number or '{Settings.menuExitCommand}' to exit)");
            Console.Write(":");

            await ProcessUserInput(Console.ReadLine());
        }

        private async Task ProcessUserInput(string input)
        {
            string cleanedInput = input.Trim();

            if (cleanedInput == Settings.menuExitCommand)
            {
                return;
            }

            try
            {
                Console.WriteLine();
                byte index = Convert.ToByte(cleanedInput);
                await commander.ExecuteCommand(index);
            }
            catch (FormatException)
            {
                Console.WriteLine("Please, enter a valid number");
            }
            catch (InvalidOperationException exc)
            {
                Console.WriteLine(exc.Message);
            }
            catch (Exception exc)
            {
                Console.WriteLine($"Unknown error detected: {exc.Message}");
            }
            finally
            {
                Console.WriteLine();
                await PrintAndAsk();
            }
        }
    }
}
