using System;
using CoolParking.BL;
using CoolParking.BL.Models;
using CoolParking.BL.Interfaces;

namespace CoolParking.Client.CommandParameters
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    internal sealed class UseParameterAttribute : Attribute
    {
        public string Key { get; }
        private string Prompt { get; }
        private string Error { get; set; }
        private bool Optional { get; }
        private Type validatorType;

        public UseParameterAttribute(string key, string textsKey, bool optional = false)
        {
            Key = key;
            Prompt = Settings.paramMessages[textsKey];
            Optional = optional;
            SetError(textsKey);
        }

        public UseParameterAttribute(string key, string textsKey, Type validatorType, bool optional = false)
        {
            Key = key;
            Prompt = Settings.paramMessages[textsKey];
            Optional = optional;
            this.validatorType = validatorType;
            SetError(textsKey);
        }

        public CommandParameterResult<T> AskAndParse<T>(Func<string, T> converter = null)
        {
            try
            {
                string input = Ask();

                if (input == "" && Optional)
                {
                    return new CommandParameterResult<T>();
                }

                T result = UnsafeParse<T>(input, converter);

                if (validatorType != null)
                {
                    IValidator<T> validatorInstance = (IValidator<T>)Activator.CreateInstance(validatorType);
                    validatorInstance.Validate(result);
                }

                return new CommandParameterResult<T>(result);
            }
            catch (ArgumentException exc)
            {
                Console.WriteLine(exc.Message);
            }
            catch
            {
                Console.WriteLine(Error);
            }

            return AskAndParse<T>(converter);
        }

        private void SetError(string key)
        {
            if (Settings.paramErrorMessages.ContainsKey(key))
            {
                Error = $"{Settings.paramErrorMessages[key]} (Parameter '{Key}')";
            }
            else
            {
                Error = "";
            }
        }

        private T UnsafeParse<T>(string input, Func<string, T> converter = null)
        {
            if (converter == null)
            {
                object result = typeof(Convert)
                    .GetMethod($"To{typeof(T).Name}", new Type[] { typeof(string) })
                    .Invoke(null, new object[] { input });

                return (T)(result);
            }

            return converter(input);
        }

        private string Ask()
        {
            Console.Write(Prompt);
            return Utils.CleanString(Console.ReadLine());
        }
    }
}
