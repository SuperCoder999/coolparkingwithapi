using System;
using CoolParking.BL.Models;

namespace CoolParking.Client.CommandParameters
{
    public sealed class CommandParameterResult<T>
    {
        private T _value;
        public bool HasValue { get; }

        public T Value
        {
            get
            {
                if (!HasValue)
                {
                    throw new InvalidOperationException(Settings.errorMessages["noParamResultValue"]);
                }

                return _value;
            }
        }

        public CommandParameterResult()
        {
            _value = default(T);
            HasValue = false;
        }

        public CommandParameterResult(T value)
        {
            _value = value;
            HasValue = true;
        }

        public CommandParameterResult(T value, bool hasValue)
        {
            _value = value;
            HasValue = hasValue;
        }
    }
}
