using System;
using System.Net.Http;
using System.Threading.Tasks;
using CoolParking.BL.DTO;
using CoolParking.BL.Models;
using CoolParking.BL.Validators;
using CoolParking.Requester;
using CoolParking.Client.CommandParameters;

namespace CoolParking.Client.Commands
{
    [UseCommand(7, "addVehicle")]
    [UseParameter("type", "vehicleType")]
    [UseParameter("balance", "vehicleBalance", typeof(BalanceValidator))]
    internal sealed class AddVehicleCommand : BaseCommand<AddVehicleCommand>
    {
        protected override async Task UnsafeInvoke()
        {
            decimal balance = GetParameter<decimal>("balance").Value;
            VehicleType type = GetParameter<VehicleType>("type", str => Vehicle.GetTypeByCleanedString(str)).Value;

            VehicleInputDTO data = new VehicleInputDTO
            {
                Id = Vehicle.GenerateRandomRegistrationPlateNumber(),
                Balance = balance,
                VehicleType = type
            };

            RequestSettings settings = new RequestSettings { method = HttpMethod.Post, body = data };
            VehicleOutputDTO vehicle = await requester.Request<VehicleOutputDTO>("vehicles", settings);

            Console.WriteLine($"Generated ID: {vehicle.Id}");
        }
    }
}
