using System;
using System.Threading.Tasks;

namespace CoolParking.Client.Commands
{
    [UseCommand(1, "getParkingBalance")]
    internal sealed class GetParkingBalanceCommand : BaseCommand<GetParkingBalanceCommand>
    {
        protected override async Task UnsafeInvoke()
        {
            decimal result = await requester.Request<decimal>("parking/balance");
            Console.WriteLine(result);
        }
    }
}
