using System;
using System.Threading.Tasks;

namespace CoolParking.Client.Commands
{
    [UseCommand(3, "getPlacesInfo")]
    internal sealed class GetPlacesInfoCommand : BaseCommand<GetPlacesInfoCommand>
    {
        protected override async Task UnsafeInvoke()
        {
            int free = await requester.Request<int>("parking/freePlaces");
            int capacity = await requester.Request<int>("parking/capacity");

            Console.WriteLine($"Capacity: {capacity}");
            Console.WriteLine($"Free places: {free}");
            Console.WriteLine($"Busy places: {capacity - free}");
        }
    }
}
