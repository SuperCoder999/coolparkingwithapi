using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using CoolParking.BL.DTO;
using CoolParking.BL.Models;

namespace CoolParking.Client.Commands
{
    [UseCommand(6, "getRegisteredVehicles")]
    internal sealed class GetRegisteredVehiclesCommand : BaseCommand<GetRegisteredVehiclesCommand>
    {
        protected override async Task UnsafeInvoke()
        {

            VehicleOutputDTO[] vehicles = await requester.Request<VehicleOutputDTO[]>("vehicles");

            IEnumerable<string> strings = from dto in vehicles
                                          select dto.ToString();

            string result = string.Join("\n", strings);
            Console.WriteLine(result);
        }
    }
}
