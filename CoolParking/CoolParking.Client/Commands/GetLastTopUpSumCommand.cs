using System;
using System.Threading.Tasks;

namespace CoolParking.Client.Commands
{
    [UseCommand(2, "getLastTopUpSum")]
    internal sealed class GetLastTopUpSumCommand : BaseCommand<GetLastTopUpSumCommand>
    {
        protected override async Task UnsafeInvoke()
        {
            decimal result = await requester.Request<decimal>("transactions/lastSum");
            Console.WriteLine(result);
        }
    }
}
