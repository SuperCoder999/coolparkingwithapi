using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using CoolParking.BL.Validators;
using CoolParking.Requester;
using CoolParking.Client.CommandParameters;

namespace CoolParking.Client.Commands
{
    [UseCommand(5, "getLogTransactions")]
    [UseParameter("count", "logTransactionsCount", typeof(PositiveIntValidator), true)]
    internal sealed class GetTransactionsFromLogCommand : BaseCommand<GetTransactionsFromLogCommand>
    {
        protected override async Task UnsafeInvoke()
        {
            CommandParameterResult<int> countResult = GetParameter<int>("count");
            Dictionary<string, string> query = null;

            if (countResult.HasValue)
            {
                int count = countResult.Value;

                if (count == 0)
                {
                    return;
                }

                query = new Dictionary<string, string> { { "count", count.ToString() } };
            }

            string result = await requester.Request<string>("transactions/all", new RequestSettings { query = query });
            Console.WriteLine(result);
        }
    }
}
