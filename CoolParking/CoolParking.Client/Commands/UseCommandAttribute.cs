using System;
using CoolParking.BL.Models;

namespace CoolParking.Client.Commands
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public sealed class UseCommandAttribute : Attribute
    {
        internal byte Index { get; }
        internal string Prompt { get; }

        internal UseCommandAttribute(byte index, string promptKey)
        {
            Index = index;
            Prompt = Settings.commandMessages[promptKey];
        }
    }
}
