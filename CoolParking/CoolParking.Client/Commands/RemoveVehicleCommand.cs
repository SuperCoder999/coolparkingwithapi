using System;
using System.Net.Http;
using System.Threading.Tasks;
using CoolParking.BL.Validators;
using CoolParking.Requester;
using CoolParking.Client.CommandParameters;

namespace CoolParking.Client.Commands
{
    [UseCommand(8, "removeVehicle")]
    [UseParameter("id", "vehicleId", typeof(VehicleIdValidator))]
    internal sealed class RemoveVehicleCommand : BaseCommand<RemoveVehicleCommand>
    {
        protected override async Task UnsafeInvoke()
        {
            string id = GetParameter<string>("id").Value.ToUpper();
            await requester.Request($"vehicles/{id}", HttpMethod.Delete);

            Console.WriteLine($"Removed {id}");
        }
    }
}
