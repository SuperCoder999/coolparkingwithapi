using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using CoolParking.BL.DTO;
using CoolParking.BL.Models;

namespace CoolParking.Client.Commands
{
    [UseCommand(4, "getLastTransactions")]
    internal sealed class GetCurrentTransactionsCommand : BaseCommand<GetCurrentTransactionsCommand>
    {
        protected override async Task UnsafeInvoke()
        {
            TransactionOutputDTO[] result = await requester.Request<TransactionOutputDTO[]>("transactions/last");

            IEnumerable<string> strings = from trans in result
                                          select TransactionInfo.FromDTO(trans).ToString();

            Console.WriteLine(string.Join("\n", strings));
        }
    }
}
