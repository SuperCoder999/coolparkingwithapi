using System;
using System.Net.Http;
using System.Threading.Tasks;
using CoolParking.BL.DTO;
using CoolParking.BL.Validators;
using CoolParking.Requester;
using CoolParking.Client.CommandParameters;

namespace CoolParking.Client.Commands
{
    [UseCommand(9, "topUpVehicle")]
    [UseParameter("id", "vehicleId", typeof(VehicleIdValidator))]
    [UseParameter("balance", "newVehicleBalance", typeof(BalanceValidator))]
    internal sealed class TopUpVehicleCommand : BaseCommand<TopUpVehicleCommand>
    {
        protected override async Task UnsafeInvoke()
        {
            string id = GetParameter<string>("id").Value.ToUpper();
            decimal balance = GetParameter<decimal>("balance").Value;

            VehicleOutputDTO vehicle = await requester.Request<VehicleOutputDTO>(
                "transactions/topUpVehicle",
                new RequestSettings
                {
                    method = HttpMethod.Put,
                    body = new TransactionTopUpDTO
                    {
                        VehicleId = id,
                        Sum = balance,
                    }
                }
            );

            Console.WriteLine($"Topped up {id}. New balance - {vehicle.Balance}");
        }
    }
}
