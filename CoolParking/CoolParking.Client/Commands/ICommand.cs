using System.Threading.Tasks;

namespace CoolParking.Client.Commands
{
    internal interface ICommand
    {
        Task Invoke();
        UseCommandAttribute GetMetadata();
        string GetStringInfo();
    }
}
