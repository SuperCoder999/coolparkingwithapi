using System;
using System.Reflection;
using System.Threading.Tasks;
using System.Collections.Generic;
using CoolParking.BL.Models;
using CoolParking.Client.Commands;

namespace CoolParking.Client.CommandLogic
{
    public sealed class CommandService : ICommandService
    {
        private readonly List<ICommand> commands;

        public CommandService()
        {
            commands = FindCommands();
        }

        private List<ICommand> FindCommands()
        {
            List<ICommand> result = new List<ICommand>();

            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (Type type in assembly.GetTypes())
                {
                    UseCommandAttribute attribute = type.GetCustomAttribute<UseCommandAttribute>();

                    if (attribute != null)
                    {
                        ICommand command = (ICommand)Activator.CreateInstance(type);
                        result.Add(command);
                    }
                }
            }

            return result;
        }

        public async Task ExecuteCommand(byte index)
        {
            ICommand command = commands.Find(c =>
            {
                UseCommandAttribute metadata = c.GetMetadata();
                return metadata.Index == index;
            });

            if (command == null)
            {
                throw new InvalidOperationException(Settings.errorMessages["notFoundCommand"]);
            }

            await command.Invoke();
        }

        public string GetCommandsStringInfo()
        {
            string result = "";
            List<ICommand> sortedCommands = new List<ICommand>(commands);

            sortedCommands.Sort((c1, c2) =>
            {
                UseCommandAttribute meta1 = c1.GetMetadata();
                UseCommandAttribute meta2 = c2.GetMetadata();

                return meta1.Index - meta2.Index;
            });

            foreach (ICommand command in sortedCommands)
            {
                result += command.GetStringInfo();
                result += "\n";
            }

            return result;
        }
    }
}
