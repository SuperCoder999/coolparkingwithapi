using System.Threading.Tasks;

namespace CoolParking.Client.CommandLogic
{
    public interface ICommandService
    {
        string GetCommandsStringInfo();
        Task ExecuteCommand(byte index);
    }
}
