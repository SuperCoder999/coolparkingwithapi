using System;
using System.Net.Http;
using System.Threading.Tasks;
using CoolParking.BL.Models;

namespace CoolParking.Requester
{
    public sealed class HttpRequester : IDisposable
    {
        private IHttpAdapter adapter;

        public HttpRequester()
        {
            adapter = new DefaultHttpAdapter();
        }

        public HttpRequester(IHttpAdapter customAdapter)
        {
            adapter = customAdapter;
        }

        public async Task<T> Request<T>(string url)
        {
            return await adapter.Request<T>(GetUrl(url), new RequestSettings());
        }

        public async Task<T> Request<T>(string url, HttpMethod method)
        {
            return await adapter.Request<T>(GetUrl(url), new RequestSettings { method = method });
        }

        public async Task<T> Request<T>(string url, RequestSettings settings)
        {
            return await adapter.Request<T>(GetUrl(url), settings);
        }

        public async Task Request(string url)
        {
            await adapter.Request(GetUrl(url), new RequestSettings());
        }

        public async Task Request(string url, HttpMethod method)
        {
            await adapter.Request(GetUrl(url), new RequestSettings { method = method });
        }

        public async Task Request(string url, RequestSettings settings)
        {
            await adapter.Request(GetUrl(url), settings);
        }

        public void Dispose()
        {
            adapter.Dispose();
        }

        private string GetUrl(string url)
        {
            return Settings.baseUrl + "/api/" + url;
        }
    }
}
