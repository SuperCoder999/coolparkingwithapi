using System.Net.Http;
using System.Collections.Generic;

namespace CoolParking.Requester
{
    public sealed class RequestSettings
    {
        public object body;
        public string authorization;
        public string contentType = "application/json";
        public string accept = "application/json";
        public HttpMethod method = HttpMethod.Get;
        public Dictionary<string, string> query;
        public Dictionary<string, string> headers;
    }
}
