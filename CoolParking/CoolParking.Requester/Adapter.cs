using System;
using System.Text;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using CoolParking.BL;
using CoolParking.BL.DTO;

namespace CoolParking.Requester
{
    public interface IHttpAdapter : IDisposable
    {
        Task Request(string url, RequestSettings settings);
        Task<T> Request<T>(string url, RequestSettings settings);
    }

    internal sealed class DefaultHttpAdapter : IHttpAdapter, IDisposable
    {
        private HttpClient client;
        private HttpClientHandler handler;

        public DefaultHttpAdapter()
        {
            handler = new HttpClientHandler();
            handler.ServerCertificateCustomValidationCallback = (sender, cert, chain, err) => true;

            client = new HttpClient(handler);
        }

        public async Task<string> RequestString(string url, RequestSettings settings)
        {
            HttpRequestMessage message = GenerateRequestMessage(url, settings);
            HttpResponseMessage response = await client.SendAsync(message);

            string stringRes = await response.Content.ReadAsStringAsync();
            string contentType = null;

            foreach (KeyValuePair<string, IEnumerable<string>> header in response.Headers)
            {
                if (header.Key.ToLower() == "content-type")
                {
                    foreach (string value in header.Value)
                    {
                        contentType = value;
                        break;
                    }
                }
            }

            if (!response.IsSuccessStatusCode)
            {
                string errorMessage = $"Request {url} failed with status {response.StatusCode}";

                try
                {
                    ErrorDTO error = Utils.JsonParse<ErrorDTO>(stringRes);
                    errorMessage = error.Message;
                }
                finally
                {
                    throw new HttpRequestException(errorMessage);
                }
            }

            return stringRes;
        }

        public async Task Request(string url, RequestSettings settings)
        {
            await RequestString(url, settings);
        }

        public async Task<T> Request<T>(string url, RequestSettings settings)
        {
            string str = await RequestString(url, settings);
            return Utils.JsonParse<T>(str);
        }

        public void Dispose()
        {
            client.Dispose();
        }

        private HttpRequestMessage GenerateRequestMessage(string url, RequestSettings settings)
        {
            string query = "";

            if (settings.query != null)
            {
                query += "?";
                int index = 0;

                foreach (KeyValuePair<string, string> item in settings.query)
                {
                    if (index > 0)
                    {
                        query += "&";
                    }

                    query += $"{item.Key}={item.Value}";
                }
            }

            HttpRequestMessage message = new HttpRequestMessage(settings.method, url + query);

            if (settings.headers != null)
            {
                foreach (KeyValuePair<string, string> header in settings.headers)
                {
                    message.Headers.Add(header.Key, header.Value);
                }
            }

            message.Headers.Add("Accept", settings.accept);

            if (settings.authorization != null)
            {
                message.Headers.Add("Authorization", $"Bearer {settings.authorization}");
            }

            if (settings.body != null)
            {
                if (settings.method == HttpMethod.Get)
                {
                    throw new InvalidOperationException("GET request doesn't support request body");
                }

                string stringBody = Utils.JsonStringify(settings.body);
                StringContent content = new StringContent(stringBody, Encoding.UTF8);

                content.Headers.ContentType = MediaTypeHeaderValue.Parse(settings.contentType);
                message.Content = content;
            }

            return message;
        }
    }
}
