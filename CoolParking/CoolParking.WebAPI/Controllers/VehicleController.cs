using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.DTO;
using CoolParking.BL.Models;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Exceptions;
using CoolParking.BL.Validators;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/vehicles")]
    public sealed class VehicleController : ControllerBase
    {
        private readonly IParkingService _parkingService;
        private readonly IValidator<string> _idValidator = new VehicleIdValidator();

        public VehicleController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet]
        public IEnumerable<VehicleOutputDTO> Get()
        {
            return from vehicle in _parkingService.GetVehicles()
                   select vehicle.ToDTO();
        }

        [HttpGet("{id}")]
        public ActionResult<VehicleOutputDTO> GetById([FromRoute] string id)
        {
            _idValidator.Validate(id);
            Vehicle vehicle = _parkingService.GetOneVehicle(id);

            if (vehicle == null)
            {
                throw new NotFoundException("Vehicle");
            }

            return vehicle.ToDTO();
        }

        [HttpPost]
        public ActionResult<VehicleOutputDTO> Create([FromBody] VehicleInputDTO dto)
        {
            Vehicle vehicle = Vehicle.FromDTO(dto);
            _parkingService.AddVehicle(vehicle);
            HttpContext.Response.StatusCode = 201;

            return vehicle.ToDTO();
        }

        [HttpDelete("{id}")]
        public void Delete([FromRoute] string id)
        {
            _idValidator.Validate(id);

            try
            {
                _parkingService.RemoveVehicle(id);
            }
            catch (ArgumentException)
            {
                throw new NotFoundException("Vehicle");
            }

            HttpContext.Response.StatusCode = 204;
        }
    }
}
