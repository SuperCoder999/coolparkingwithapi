#nullable enable

using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.DTO;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Validators;
using CoolParking.BL.Exceptions;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/transactions")]
    public sealed class TransactionController : ControllerBase
    {
        private readonly IParkingService _parkingService;
        private readonly IValidator<string> _idValidator = new VehicleIdValidator();
        private readonly IValidator<decimal> _balanceValidator = new BalanceValidator();

        public TransactionController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet("last")]
        public IEnumerable<TransactionOutputDTO> GetLast()
        {
            return from trans in _parkingService.GetLastParkingTransactions()
                   select trans.ToDTO();
        }

        [HttpGet("all")]
        public ActionResult<string> GetAll([FromQuery] int? count)
        {
            if (count != null)
            {
                string[] strings = _parkingService.ReadFromLog().Split("\n");
                return string.Join("\n", strings.Skip(Math.Max(strings.Length - (int)count - 1, 0)).Take((int)count));
            }

            try
            {
                return _parkingService.ReadFromLog();
            }
            catch
            {
                throw new NotFoundException("Log file");
            }
        }

        [HttpGet("lastSum")]
        public ActionResult<decimal> GetLastSum()
        {
            return _parkingService.GetLastParkingTopUpSum();
        }

        [HttpPut("topUpVehicle")]
        public ActionResult<VehicleOutputDTO> TopUpVehicle([FromBody] TransactionTopUpDTO data)
        {
            _idValidator.Validate(data.VehicleId);
            _balanceValidator.Validate(data.Sum);

            try
            {
                _parkingService.TopUpVehicle(data.VehicleId, data.Sum);
            }
            catch (ArgumentException)
            {
                throw new NotFoundException("Vehicle");
            }

            return _parkingService.GetOneVehicle(data.VehicleId).ToDTO();
        }
    }
}
