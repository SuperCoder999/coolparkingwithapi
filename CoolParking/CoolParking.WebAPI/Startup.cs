using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using CoolParking.WebAPI.Middleware;
using components = CoolParking.WebAPI.StartupComponents;

namespace CoolParking.WebAPI
{
    public sealed class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers(options =>
            {
                options.InputFormatters.RemoveType<SystemTextJsonInputFormatter>();
                options.InputFormatters.Add(new components::NewtonsoftJsonInputFormatter());

                options.OutputFormatters.RemoveType<SystemTextJsonOutputFormatter>();
                options.OutputFormatters.Add(new components::NewtonsoftJsonOutputFormatter());
            });

            services.AddSingleton<IParkingService, ParkingService>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            ConfigureConditional(app, env);
            ConfigureCertain(app, env);
        }

        private void ConfigureDevelopment(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseDeveloperExceptionPage();
        }

        private void ConfigureProduction(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseHsts();
        }

        private void ConfigureConditional(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                ConfigureDevelopment(app, env);
            }
            else
            {
                ConfigureProduction(app, env);
            }
        }

        private void ConfigureCertain(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseMiddleware<ErrorMiddleware>();
            app.UseRouting();
            app.UseHttpsRedirection();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
