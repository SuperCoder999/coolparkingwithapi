using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using CoolParking.BL.Exceptions;
using CoolParking.BL.DTO;

namespace CoolParking.WebAPI.Middleware
{
    public sealed class ErrorMiddleware
    {
        private readonly RequestDelegate _next;

        public ErrorMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next.Invoke(context);
            }
            catch (Exception exc)
            {
                HttpException httpExc = HttpException.From(exc);

                context.Response.StatusCode = httpExc.Status;
                await context.Response.WriteAsJsonAsync(new ErrorDTO { Message = httpExc.Message });
            }
        }
    }
}
