using System.Text;
using System.Threading.Tasks;
using Microsoft.Net.Http.Headers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using CoolParking.BL;

namespace CoolParking.WebAPI.StartupComponents
{
    public sealed class NewtonsoftJsonOutputFormatter : OutputFormatter
    {
        public NewtonsoftJsonOutputFormatter() : base()
        {
            SupportedMediaTypes.Clear();
            SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("application/json"));
        }

        public override bool CanWriteResult(OutputFormatterCanWriteContext context)
        {
            return base.CanWriteResult(context);
        }

        public override Task WriteResponseBodyAsync(OutputFormatterWriteContext context)
        {
            HttpResponse response = context.HttpContext.Response;
            string result = Utils.JsonStringify(context.Object);
            response.Body.WriteAsync(Encoding.UTF8.GetBytes(result)).GetAwaiter().GetResult();

            return Task.FromResult(response);
        }
    }
}
