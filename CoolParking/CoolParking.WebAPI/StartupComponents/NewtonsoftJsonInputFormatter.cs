using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Net.Http.Headers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using CoolParking.BL;

namespace CoolParking.WebAPI.StartupComponents
{
    public sealed class NewtonsoftJsonInputFormatter : InputFormatter
    {
        public NewtonsoftJsonInputFormatter() : base()
        {
            SupportedMediaTypes.Clear();
            SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("application/json"));
        }

        public override bool CanRead(InputFormatterContext context)
        {
            return base.CanRead(context);
        }

        public override Task<InputFormatterResult> ReadRequestBodyAsync(InputFormatterContext context)
        {
            Type type = context.ModelType;
            HttpRequest request = context.HttpContext.Request;
            string body;

            using (StreamReader bodyReader = new StreamReader(request.Body))
            {
                body = bodyReader.ReadToEndAsync().Result;
            }

            dynamic result = Utils.JsonParse(body, type);
            return InputFormatterResult.SuccessAsync(result);
        }
    }
}
