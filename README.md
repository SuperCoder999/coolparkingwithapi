# Cool Parking with Web API

**BSA 2021 .NET third homework**

**To run it _.NET 5_ is required**

<br />
<br />

Before all other operations:

```
cd <project_root>
```

To run Web API:

```
cd CoolParking/CoolParking.WebAPI
dotnet run
```

To run Client Console App:

```
cd CoolParking/CoolParking.Client
dotnet run
```

## Project dependencies

- Newtonsoft.Json

## Dev dependencies

- VSCode extension vscode-solution-explorer

## Tips

- Transactions log file is located at <project_root>/CoolParking/CoolParking.WebAPI/bin/Debug/net5.0/Transactions.log
- HttpClient wrapper is CoolParking.Requester project
